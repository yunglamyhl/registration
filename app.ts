import * as express from 'express';
import * as expressSession from 'express-session'
import * as http from 'http';
import * as bodyParser from 'body-parser';
import * as fs from 'fs';


const app = express()
const server = new http.Server(app);
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}))
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());// JSON.parse
app.use(express.static('./protected'));



const users = JSON.parse(fs.readFileSync('users.json', 'utf8'))

app.post('/register', async (req, res) =>{
    //app.get req.query.username
    //app.get req.query.password
    console.log(req.body)
    users.push({username:req.body.username, password: req.body.password});
    await fs.promises.writeFile('users.json', JSON.stringify(users));
    res.json({isSuccess:true});
    
})


server.listen(8080, function () {
    console.log("Ready")
})