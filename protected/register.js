

document.querySelector('.form-horizontal').addEventListener('submit', function (event) {
    event.preventDefault();


    let password = document.querySelector('#password').value
    let cpassword = document.querySelector('#password_confirm').value
    let name = document.querySelector('#username').value

    async function confirmPassword(a, b, c) {
        console.log("run")
        if (b !== c) {
            console.log("Inconsistent Password")
            window.alert("Inconsistent Password");
        } else {
            res = await fetch('/register',
                {
                    method: "POST", // Specific your HTTP method
                    // mode: "cors", // Useful for accessing different hostname
                    // credentials: "same-origin", // same-origin will send the cookies by default
                    headers: {
                        // Specify any HTTP Headers Here
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    // redirect: "follow", // Specify what to do if a redirect is encountered
                    body: JSON.stringify({ username: a, password: b }), // Specify the Request Body
                }
            );
            const result = await res.json(); // JSON.parse
            location.href=('/profile.html')
        }
    }

 

    confirmPassword(name,password, cpassword);

})